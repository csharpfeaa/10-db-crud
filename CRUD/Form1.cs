﻿using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD
{
    public partial class Form1 : Form
    {
        OleDbConnection dbConnection;
        DataTable dtApartamente;
        int currentRecordIndex = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // deschidem conexiunea
            dbConnection = new OleDbConnection();
            dbConnection.ConnectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\Marian\\Documents\\apartamente.accdb";

            try
            {
                dbConnection.Open();
            }
            catch (Exception exception)
            {
                MessageBox.Show(exception.Message);
                this.Close(); // daca nu am reusit sa ne conectam la baza de date, inchidem formularul
            }

            fetchApartamenteData();
            loadApartamenteRecord(currentRecordIndex);
        }

        /*
         * Aducem toate inregistrarile din tabela apartamente
         */
        private void fetchApartamenteData()
        {
            OleDbCommand dbCommand = new OleDbCommand();
            dbCommand.CommandText = "SELECT * FROM apartamente";
            dbCommand.Connection = dbConnection;
            
            // rulam comanda
            OleDbDataReader dbDataReader = dbCommand.ExecuteReader();
            dtApartamente = new DataTable("apartamente");
            dtApartamente.Load(dbDataReader);
               
        }

        /*
         * Afisam apartamentul conform indexului cerut
         */
        private void loadApartamenteRecord(int index)
        {
            if (dtApartamente.Rows.Count == 0)
            {
                MessageBox.Show("Nu sunt inregistrari de afisat!");
                txtCodApartament.Clear();
                txtNumePropr.Clear();
                txtOras.Clear();
                txtPret.Clear();

                return;
            }
            DataRow row = dtApartamente.Rows[index];
            txtCodApartament.Text = row["codApartament"].ToString();
            txtNumePropr.Text = row["numeProprietar"].ToString();
            txtOras.Text = row["oras"].ToString();
            txtPret.Text = row["pretEstimativ"].ToString();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            currentRecordIndex = (currentRecordIndex + 1) % dtApartamente.Rows.Count;
            loadApartamenteRecord(currentRecordIndex);
        }

        private void btnPrevious_Click(object sender, EventArgs e)
        {
            currentRecordIndex--;

            if (currentRecordIndex < 0)
                currentRecordIndex = dtApartamente.Rows.Count - 1;

            loadApartamenteRecord(currentRecordIndex);
        }

        private void btnClearForm_Click(object sender, EventArgs e)
        {
            txtCodApartament.Clear();
            txtNumePropr.Clear();
            txtOras.Clear();
            txtPret.Clear();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (txtCodApartament.Text == "")
            {
                MessageBox.Show("Te rugam sa introduci codul apartamentului!");
                return;
            }

            if (txtNumePropr.Text == "")
            {
                MessageBox.Show("Te rugam sa introduci numele proprietarului!");
                return;
            }

            if (txtOras.Text == "")
            {
                MessageBox.Show("Te rugam sa introduci orasul!");
                return;
            }

            if (!int.TryParse(txtPret.Text, out int pret))
            {
                MessageBox.Show("Te rugam sa introduci un pret valid!");
                return;
            }


            // toate campurile sunt valide, adaugam in baza de date
            OleDbCommand cmdInsert = new OleDbCommand();
            cmdInsert.CommandText = "INSERT INTO apartamente VALUES(@codApartament, @numeProprietar, @oras, @pretEstimativ)";
            cmdInsert.Connection = dbConnection;
            cmdInsert.Parameters.AddWithValue("@codApartament", txtCodApartament.Text.ToString());
            cmdInsert.Parameters.AddWithValue("@numeProprietar", txtNumePropr.Text.ToString());
            cmdInsert.Parameters.AddWithValue("@oras", txtOras.Text.ToString());
            cmdInsert.Parameters.AddWithValue("@pretEstimativ", pret);

            try
            {
                int randuriInserate = cmdInsert.ExecuteNonQuery();

                if (randuriInserate > 0)
                {
                    MessageBox.Show("Apartamentul a fost adaugat cu succes!");
                }
                else
                {
                    MessageBox.Show("Nici un rand nu a fost inserat!");
                }
            }
            catch(Exception err)
            {
                MessageBox.Show("A aprarut o eroare: " + err.Message);
            }

            // dupa ce am adaugat un nou rand, aducem din nou datele din DB
            fetchApartamenteData();
            currentRecordIndex = dtApartamente.Rows.Count - 1;
            loadApartamenteRecord(currentRecordIndex);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            // stergem inregistrarea pe baza cod apartament
            if (txtCodApartament.Text == "")
            {
                MessageBox.Show("Te rugam introdu un cod de apartament!");
                return;
            }

            DialogResult result = MessageBox.Show("Esti singur ca vrei sa stergi apartamentul " + txtCodApartament.Text + "?", "Confirma", MessageBoxButtons.YesNo);

            if (result == DialogResult.No)
            {
                return;
            }

            // a apasat pe da, stergem inregistrarea
            OleDbCommand cmdStergere = new OleDbCommand();
            cmdStergere.CommandText = "DELETE FROM apartamente WHERE codApartament = @codApartament";
            cmdStergere.Connection = dbConnection;
            cmdStergere.Parameters.AddWithValue("@codApartament", txtCodApartament.Text);

            try
            {
                // metoda ExecuteNonQuery() returneaza cate randuri din baza de date au fost afectate
                int rezultat = cmdStergere.ExecuteNonQuery();

                if (rezultat > 0)
                {
                    MessageBox.Show("Apartamentul a fost sters.");
                }
                else
                {
                    MessageBox.Show("Ceva nu a mers cum trebuie?!");
                }
            }
            catch (Exception eroare)
            {
                MessageBox.Show(eroare.ToString());
            }

            // actualizam datele in datatable
            fetchApartamenteData();
            currentRecordIndex = 0;
            loadApartamenteRecord(currentRecordIndex);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            // codul ap nu avem voie sa-l modificam
            if (txtCodApartament.Text == "" || txtCodApartament.Text !=  dtApartamente.Rows[currentRecordIndex]["codApartament"].ToString())
            {
                MessageBox.Show("Codul apartamentului nu poate fi modificat!");
                return;
            }

            if (txtNumePropr.Text == "")
            {
                MessageBox.Show("Te rugam sa introduci numele proprietarului!");
                return;
            }

            if (txtOras.Text == "")
            {
                MessageBox.Show("Te rugam sa introduci orasul!");
                return;
            }

            if (!int.TryParse(txtPret.Text, out int pret))
            {
                MessageBox.Show("Te rugam sa introduci un pret valid!");
                return;
            }

            OleDbCommand cmdInsert = new OleDbCommand();
            cmdInsert.CommandText = "UPDATE apartamente SET numeProprietar = @numeProprietar, oras = @oras, pretEstimativ = @pretEstimativ WHERE codApartament = @codApartament";
            cmdInsert.Connection = dbConnection;
            cmdInsert.Parameters.AddWithValue("@numeProprietar", txtNumePropr.Text.ToString());
            cmdInsert.Parameters.AddWithValue("@oras", txtOras.Text.ToString());
            cmdInsert.Parameters.AddWithValue("@pretEstimativ", pret);
            cmdInsert.Parameters.AddWithValue("@codApartament", txtCodApartament.Text.ToString());

            try
            {
                int randuriInserate = cmdInsert.ExecuteNonQuery();

                if (randuriInserate > 0)
                {
                    MessageBox.Show("Apartamentul a fost modificat cu succes!");
                }
                else
                {
                    MessageBox.Show("Nici un rand nu a fost inserat!");
                }
            }
            catch (Exception err)
            {
                MessageBox.Show("A aprarut o eroare: " + err.Message);
            }

            // dupa ce am adaugat un nou rand, aducem din nou datele din DB si afisam primul rand
            fetchApartamenteData();
            loadApartamenteRecord(currentRecordIndex); // de data aceasta nu trebuie sa resetam indexul
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string cuvantCautat = Interaction.InputBox("Cauta dupa cod apartament", "Formular cautare");

            OleDbCommand cmdCautare = new OleDbCommand();
            cmdCautare.CommandText = "SELECT * FROM apartamente WHERE codApartament = @codApartament";
            cmdCautare.Connection = dbConnection;
            cmdCautare.Parameters.AddWithValue("@codApartament", cuvantCautat);

            // rulam comanda
            OleDbDataReader dbDataReader = cmdCautare.ExecuteReader();
            DataTable dtRezultate = new DataTable("rezultate");
            dtRezultate.Load(dbDataReader);

            if (dtRezultate.Rows.Count > 0)
            {
                MessageBox.Show($"Am gasit apartamentul cautat.\n\n" +
                   $"Cod: {dtRezultate.Rows[0]["codApartament"].ToString()}\n" +
                   $"Prop: {dtRezultate.Rows[0]["numeProprietar"].ToString()}\n" +
                   $"Oras: {dtRezultate.Rows[0]["oras"].ToString()}\n" +
                   $"Pret: {dtRezultate.Rows[0]["pretEstimativ"].ToString()}\n");
            }
            else
            {
                MessageBox.Show("Nu am gasit nici un apartament pentru codul introdus.");
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            dbConnection.Close();
        }
    }
}
