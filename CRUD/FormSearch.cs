﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.OleDb;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CRUD
{
    public partial class FormSearch : Form
    {
        String connectionString = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=C:\\Users\\Marian\\Documents\\apartamente.accdb";
        DataTable dtApartamente;

        public FormSearch()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            SearchApartamente();
        }

        private void SearchApartamente()
        {
            if (txtKeyword.Text == "")
            {
                MessageBox.Show("Introduceti un oras.");
                return; // oprim executia
            }


            // folosind <<using>> nu mai trebuie sa inchidem manual conexiunea la baza de date
            // cand se termina blocul <<using>>, sau in cazul unei erori, conexiunea va fi inchisa automat
            using (OleDbConnection dbConnection = new OleDbConnection(connectionString))
            {
                OleDbCommand dbCommand = new OleDbCommand();
                dbCommand.CommandText = "SELECT * FROM apartamente WHERE LCASE(oras) = @oras";
                dbCommand.Connection = dbConnection;
                dbCommand.Parameters.AddWithValue("oras", txtKeyword.Text.ToLower());

                try
                {
                    dbConnection.Open();
                    OleDbDataReader dataReader = dbCommand.ExecuteReader();

                    dtApartamente = new DataTable("apartamente");
                    dtApartamente.Load(dataReader);

                    ShowRecords();
                }
                catch (Exception err)
                {
                    MessageBox.Show(err.Message);
                }
            }
        }

        private void ShowRecords()
        {
            // resetam lista
            lstApartamente.Items.Clear();

            // afisam rezultatele
            for (int i = 0; i < dtApartamente.Rows.Count; i++)
            {
                DataRow row = dtApartamente.Rows[i];
                String randAfisare = row["codApartament"].ToString() + ": " + row["numeProprietar"].ToString() + ", " + row["oras"].ToString() + ", " + row["pretEstimativ"].ToString();

                lstApartamente.Items.Add(randAfisare);
            }

            // actualizam numarul de rezultate gasite din interfata
            lblSearchCount.Text = dtApartamente.Rows.Count.ToString();
        }

        private void txtKeyword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char) 13)
            {
                SearchApartamente();
            }
        }
    }
}
